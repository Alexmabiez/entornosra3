//necesitamos el import javar.util.Scanner para leer por teclado.
import java.util.Scanner;
public class Main {
		 /*
		 * Clase para refactorizar
		 * Codificaci�n UTF-8
		 * Paquete refactorizacion
		 * Se debe comentar todas las refactorizaciones realizadas,
		 * por comentarios de una linea de bloque.
		 */
		 /**
		 * 
		 * @author fvaldeon
		 * @desde el 31-01-2018
		 **/
		 public static class refactorizar {
			 static Scanner in=new Scanner(System.in);
			 	//Las variables han de estar declaradas por separado.
			 
			 final static String cad = "Bienvenido al programa";
				static String cadena2;
			 	public static void main(String[] args) {
			 		System.out.println("Introduce tu nombre");
			 		cadena2=in.nextLine();
			 		double a,b;
					a=3.1415; b=16;
					a=7; b=16;
			 		int numeroc = 25;
			 		//Los if deben ir entre llaves.
			 		//Ponemos los parentesis entre las operaciones.
			 		if( a > b|| numeroc % 5!=0&&(( numeroc * 3 )-1)> b / numeroc ){
			 			System.out.println("Se cumple la condici�n");
			 		}
			 		//Casteo
			 		numeroc = (int) (a+b*numeroc+b/a);
			 		
			 		//Los Arrays deben declarar los corchetes despues de la clase de variable.
			 		//Cambiamos el nombre del array para que sea m�s descriptivo y almacenar los 
			 		// datos como constantes
			 		String[] diasDeLaSemana = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes",
			 				"Sabado", "Domingo"	};	
			 		
			 	mostrarDiasDeLaSemana(diasDeLaSemana);
			 	}
			 	static void mostrarDiasDeLaSemana(String[] diasDeLaSemana)
			 	{
			 	//Las lineas no deben ser de m�s de 100 caracteres para mejorar la lectura
			 	for(int i = 0 ;i < 7 ;i++ ) 
			 	{
			 		System.out.println("El dia de la semana en el que te encuentras ["+(i+1)
			 				+"-7]"
			 				+ " es el dia: "+diasDeLaSemana[i]);
			 		}
			 	}
			 	
			 }
			}	